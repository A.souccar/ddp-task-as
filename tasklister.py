import ifcopenshell
import barread
from pprint import pprint


model=ifcopenshell.open('/Users/orhunozcan/repos/DDP/DPPModelEdited.ifc')

spacecode=barread.qrcodecapture()

relatedspaceelement=model.by_guid(spacecode)

RelSpatialStructures=model.by_type('IfcRelContainedInSpatialStructure')

for RelSpatialStructure in RelSpatialStructures:
    if RelSpatialStructure.RelatingStructure==relatedspaceelement:
        a=RelSpatialStructure
        aelements=RelSpatialStructure.RelatedElements

Tasks=model.by_type('IfcTask')
TaskAssigns=model.by_type('IfcRelAssignsToProduct')

QueriedTasks=[]

for assign in TaskAssigns:
    for aelement in aelements:
        if aelement==assign.RelatingProduct:
            QueriedTasks.append(assign.RelatedObjects)

pprint(QueriedTasks)

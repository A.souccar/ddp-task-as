import ifcopenshell
from pprint import pprint
import pandas as pd
from pandas import DataFrame
import openpyxl
import csv


model=ifcopenshell.open('/Users/orhunozcan/repos/DDP/DDP1.ifc')

RelSpaceElements=model.by_type('IfcRelContainedInSpatialStructure')


for RelSpaceElement in RelSpaceElements:
        x=RelSpaceElement.RelatedElements
        for y in x:
                if y is not None:
                        if y.is_a('IfcWall'):
                                t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Brick Laying',Status='Not Complete')              
                                t2=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Plaster',Status='Not Complete')
                                t3=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Paint',Status='Not Complete')
                                model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1,t2,t3])
                        if y.is_a('IfcColumn'):
                                t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Rebar',Status='Not Complete')
                                t2=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Formwork',Status='Not Complete')
                                t3=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Concrete',Status='Not Complete')
                                model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1,t2,t3])
                        if y.is_a('IfcSlab'):
                                t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='FormWork',Status='Not Complete')
                                t2=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Rebar',Status='Not Complete')
                                t3=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Concrete',Status='Not Complete')
                                model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1,t2,t3])
                        if y.is_a('IfcDoor'):
                                t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Door Installation',Status='Not Complete')
                                model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1])
                        if y.is_a("IfcWindow"):
                                t1=model.create_entity('IfcTask',GlobalId=ifcopenshell.guid.new(),Name='Window Installation', Status='Not Complete')
                                model.create_entity('IfcRelAssignsToProduct', GlobalId=ifcopenshell.guid.new(),RelatingProduct=y,RelatedObjects=[t1])

model.write('DPPModelEdited.ifc')    

